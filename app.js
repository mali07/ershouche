var aldstat = require("./utils/ald-stat.js");
App({
  onLaunch: function (options) {
    wx.login({
      success: function (res) {
        if (res.code) {
          //发起网络请求
          console.log('用户登录！');
          wx.request({
            url: 'https://wecar.58.com/xcx/jscode2session',
            data: {
              code: res.code
            },
            success:function(res){
             if(res.data.state == 0){
               var content=JSON.parse(res.data.content);
               try {
                 wx.setStorageSync('openid', content.openid);
               } catch (e) {
                 console.log('error');
               }
             }
            },
            fail:function(){
              console.log('获取openid失败！');
            }
          })
        } else {
          console.log('用户登录态失败！' + res.errMsg)
        }
      }
    });
  }
})