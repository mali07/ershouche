import * as echarts from '../../ec-canvas/echarts';
var util = require('../../utils/util.js')


const app = getApp();
var myCharts = null;
//根据年份获得推荐价格
function getsupprice(num, arr1, arr2) {
  var price = 0;
  for (var index in arr1) {
    if (arr1[index] == num) {
      price = arr2[index];
    }
  }
  return price;
}

//初始化echart
function initChart(canvas, width, height) {
  myCharts = echarts.init(canvas, null, {
    width: width,
    height: height
  });
  canvas.setChart(myCharts);
  var option = {};
  myCharts.setOption(option);
  return myCharts;
}

//更新myecharts
function updatemyecharts(optionData, _type, isiOS) {

  var baseColor = '#DBAC62';
  if (_type == 1) {
    baseColor = '#7799DD';
  } else {
    baseColor = '#DBAC62';
  }

  var Xdata = optionData.bzlPriceKeyList || [];
  var valuedata = optionData.bzlPriceValueList || [];
  var newCarPrice = optionData.newCarPrice;
  var recommendYear = optionData.recommendYear;

  // console.log(newCarPrice);
  // console.log(getsupprice(recommendYear, Xdata, valuedata));

  var option = {
    title: {
      text: '单位：万',
      right: '0',
      top: '-2',
      textStyle: {
        color: '#9E9EA8',
        fontFamily: 'PingFangSC-Regular',
        fontSize: 12
      }
    },
    grid: {
      show: false,
      top: '18%',
      left: '0%',
      right: '0%',
      bottom: '0',
      containLabel: true
    },
    lineStyle: {
      color: baseColor,
      width: 6
    },
    xAxis: {
      type: 'category',
      // type: 'value',
      // boundaryGap: false,
      boundaryGap: ['0%', '10%'],
      data: Xdata,
      axisLine: {    // 轴线
        show: true,
        lineStyle: {
          color: '#EAEAEA',
          type: 'solid',
          width: 1
        }
      },
      axisLabel: {
        show: true,
        interval: 0,    // {number}
        textStyle: {
          color: '#9E9EA8',
          fontFamily: 'PingFangSC-Regular',
          fontSize: 12
        }
      },
      axisTick: {    // 轴标记
        show: true,
        lineStyle: {
          type: 'solid',
          width: 0
        }
      }
    },
    yAxis: {
      type: 'value',
      axisLine: {    // 轴线
        show: true,
        lineStyle: {
          color: '#EAEAEA',
          type: 'solid',
          width: 1
        }
      },
      splitLine: {
        show: true,
        lineStyle: {
          color: '#EAEAEA',
          type: 'solid',
          width: 1
        }
      },
      axisLabel: {
        show: true,
        interval: 'auto',    // {number}
        textStyle: {
          color: '#9E9EA8',
          fontFamily: 'PingFangSC-Regular',
          fontSize: 12
        }
      },
      axisTick: {    // 轴标记
        show: true,
        lineStyle: {
          type: 'solid',
          width: 0
        }
      }
    },
    series: [
      {
        type: 'line',
        smooth: false,
        // yAxisIndex: valuedata[6],
        data: valuedata,
        markPoint: {
          clickable: true,
          symbol: 'pin',
          symbolSize: 30,
          data: [
            { value: '指导价', yAxis: newCarPrice },//通过yAxis来控制markPoint的位
            { value: '推荐', yAxis: getsupprice(recommendYear, Xdata, valuedata) }
          ],
          itemStyle: {
            normal: {
              color: '#A4BDE3',
              borderColor: baseColor,
              borderWidth: 1,
              label: {
                show: true,
                position: 'top'
              }
            }
          },
          textStyle: {
            color: '#9E9EA8',
            fontFamily: 'PingFangSC-Regular',
            fontSize: 8
          }
        },
        lineStyle: {
          width: 4
        },
        symbolSize: 8,//拐点大小
        itemStyle: {
          normal: {
            color: baseColor
          }
        },
        label: {
          show: true,
          position: 'top'
        }
      }
    ]
  };

  if (!isiOS) {  //ios 环境下 不显示渐变色（ 因为目前echarts 在ios微信中的bug ）
    option.series[0].areaStyle = {
      color: new echarts.graphic.LinearGradient(
        0, 0, 0, 1,
        [
          { offset: 0, color: baseColor },
          { offset: 1, color: '#ffffff' }
        ]
      )
    }

    console.log(option)
    myCharts.setOption(
      option
    );
    return
  }

  myCharts.setOption(
    option
  );


}

//获取车型大数据
function getModelBigData(obj, url) {
  wx.request({
    url: url,
    header: {
      'content-type': 'application/json'
    },
    success: function (res) {

      //计算宽度百分比
      var modelBigData = res.data.data;
      var max = null;
      for (var i = 0; i < modelBigData.length; i++) {
        for (var j = 0; j < modelBigData[i].data.length; j++) {
          if (j == 0) {
            max = modelBigData[i].data[j].infoNum || modelBigData[i].data[j].price || modelBigData[i].data[j].cvr;
          }
          var item = modelBigData[i].data[j].infoNum || modelBigData[i].data[j].price || modelBigData[i].data[j].cvr;
          modelBigData[i].data[j].bfb = util.toPercent(item, max);
        }
      }


      obj.setData({
        modelBigData: modelBigData
      })
      // console.log(modelBigData)
    },
    error: function (msg) {
      console.log(msg)
    }
  })

}

// 估价结果数据
function getBuyPageData(obj, _type, data, callback) {
  var miles = data.miles;
  var cityId = data.cityId;
  var modelValue = data.modelValue;
  var regTime = data.regTime;
  var key = util.md5_cmn(modelValue);

  var _url = 'https://carprice.m.58.com/appapi/getBuyPageData?platform=M&key=' + key + '&miles=' + miles + '&version=1.1.0&uid=0&buytypeValue=' + _type + '&regTime=' + regTime + '&cityId=' + cityId + '&modelValue=' + modelValue;
  console.log(_url)
  wx.request({  //获取估价信息
    url: _url,
    header: {
      'content-type': 'application/json'
    },
    success: function (res) {
      wx.hideLoading();
      obj.setData({
        newCarprice: res.data.newCarprice || null,
        goldEstimator: res.data.goldEstimator || false,
        allData: res.data,
        logoImgUrl: res.data.initModelInfo.picUrl,
        defaultpriceResul: res.data.priceResultKey.Middle,
        modelBigDataUrl: res.data.modelBigData.url
      })
      callback && callback(res.data);
    }
  })
}

Page({
  data: {
    resultType: 0,   //估价结果种类  默认买车
    showFixedHeader: false, //头部隐藏tab 默认隐藏
    checkedIndex: 0,
    goldEstimator: [],  //推荐估价师
    ckCheckedIndex: 1,  //默认车况
    defaultpriceResul: {},  //默认显示
    modelBigData: '',  //车型大数据
    modelBigDataUrl: '',   //车型大数据接口url
    modelBigDataWidth: [], //车型大数据item 宽度百分比
    platformHot: false,  //市场热度toggle 默认隐藏
    cxBigData: false,   //车型大数据toggle 默认隐藏
    showRank: false,    //采销车商排行 默认隐藏
    brandTxt: '',
    distance: '',
    city: '',
    cityId: null,
    modelValue: '',
    regTime: '',
    carType: ['车况好', '车况中', '车况差'],
    newCarprice: {},
    logoImgUrl: '',
    allData: {},   //结果页全部数据
    isiOS: false,
    ec: {
      onInit: initChart
    }
  },
  // onPageScroll: function (res) {
  // this.setData({
  //   showFixedHeader: res.scrollTop >= 256
  // })
  // if (res.scrollTop >= 256) {
  //   wx.setNavigationBarTitle({
  //     title: this.data.brandTxt  //页面标题
  //   })
  // } else {
  //   wx.setNavigationBarTitle({
  //     title: '估价结果'
  //   })
  // }
  // },
  bindAreaTap: function () {   //点击所在城市
    wx.navigateTo({
      url: '../area/area'
    })
  },
  tabTap: function (e) {   //买车 卖车 切换
    var index = e.target.dataset.index;
    var that = this;
    this.setData({
      checkedIndex: index,
      ckCheckedIndex: 1
    })

    updatemyecharts(that.data.allData.bzlData, index, that.data.isiOS);


    var sellcardata = wx.getStorageSync('sellcardata') || '';
    if (index == 1 && sellcardata == '') {
      //获取卖车估价结果
      getBuyPageData(that, 0, {
        miles: that.data.distance,
        cityId: that.data.cityId,
        modelValue: that.data.modelValue,
        regTime: that.data.regTime
      }, function (data) {
        var modelBigDataUrl = that.data.modelBigDataUrl;
        getModelBigData(that, modelBigDataUrl);
        wx.setStorageSync("sellcardata", data)
      });
      return;
    }

    //从本地取已有估价数据
    var buycardata = wx.getStorageSync('buycardata');
    var sellcardata = wx.getStorageSync('sellcardata');
    var _data = null;
    if (index == 0) {
      _data = buycardata;
    } else {
      _data = sellcardata;
    }
    that.setData({
      newCarprice: _data.newCarprice || null,
      goldEstimator: _data.goldEstimator || false,
      allData: _data,
      defaultpriceResul: _data.priceResultKey.Middle,
      modelBigDataUrl: _data.modelBigData.url
    })




  },
  changeType: function (e) {  //切换车况
    var index = e.currentTarget.dataset.index;
    // console.log(this.data.allData)

    var priceResult = null;
    if (index == 0) { //好
      priceResult = this.data.allData.priceResultKey.High
    } else if (index == 1) {  //中
      priceResult = this.data.allData.priceResultKey.Middle
    } else if (index == 2) {  //差
      priceResult = this.data.allData.priceResultKey.Low
    }
    this.setData({
      ckCheckedIndex: index,
      defaultpriceResul: priceResult
    })
  },
  togglePlatformHot: function () {
    var platformHot = this.data.platformHot;

    // console.log(platformHot);
    this.setData({
      platformHot: !platformHot
    })
  },
  toggleCXBigData: function () {
    var cxBigData = this.data.cxBigData;
    this.setData({
      cxBigData: !cxBigData
    })
  },
  toggleRank: function () {
    var showRank = this.data.showRank;
    this.setData({
      showRank: !showRank
    })
  },
  makecall: function (e) {
    var num = e.currentTarget.dataset.number;
    util.makePhoneCall(num);
  },
  onLoad: function (options) {

    var that = this;
    if (util.isiOS()) {
      that.setData({
        isiOS: true
      })
    }

    console.log(options.distance)

    wx.showLoading();  //show loading
    wx.setStorageSync("buycardata", '');  //清楚本地数据
    wx.setStorageSync("sellcardata", '');

    var chexingValue = wx.getStorageSync("chexingValue");
    var date = wx.getStorageSync("year") + wx.getStorageSync("month");
    var date = date.split("年");
    var month = date[1].split("月");
    if (month[0].length == 1) {
      month[0] = '0' + month[0]
    }
    var date = date[0] + month[0];
    var distance;
    if (options.distance == "") {
       distance = 0;
    } else {
      distance = options.distance.replace("_", ".");
    }
    var cityourparam = wx.getStorageSync("cityourparam")

    that.setData({
      brandTxt: options.brandTxt,
      date: options.date,
      distance: distance,
      city: options.city,
      cityId: cityourparam,
      modelValue: chexingValue,
      regTime: date
    })

    //获取买车估价数据
    getBuyPageData(that, 1, {
      miles: distance,
      cityId: cityourparam,
      modelValue: chexingValue,
      regTime: date
    }, function (data) {
      var modelBigDataUrl = that.data.modelBigDataUrl;
      getModelBigData(that, modelBigDataUrl);

      //绘制echarts
      updatemyecharts(data.bzlData, 0, that.data.isiOS);

      wx.setStorageSync("buycardata", data)
    });
  },
  onShareAppMessage: function () {
    return {
      title: '58估车价',
      path: '/pages/index/index'
    }
  },
  bindGoErshoucheList: function () {
    wx.navigateTo({
      url: '/pages/list/list',
      success: function(res){
        // success
      },
      fail: function() {
        // fail
      },
      complete: function() {
        // complete
      }
    })
  }
})
