//index.js
//获取应用实例
var app = getApp()
Page({
  data: {
      storage:'',
      chexiArray:"",
      chexingArray:"",
      brandArray:"",
      brandArray0:"",
      brandColor:'-1',
      chexiColor:'-1',
      chexingColor:'-1',
      animationData: {},
      animationData1: {},
      endDate: new Date().getFullYear()+'-'+ new Date().getMonth()+'-'+ new Date().getDay(),
      scrollTop: 100,
      toView: 'A1',
      scrollHeight:300
  },
  goHash:function (e) {
      var hash = e.currentTarget.dataset.hash
      this.setData({
          toView: hash
      })
  },
  //事件处理函数
  bindBackTap0: function(){
    wx.navigateBack({
      delta:1
    })
  },
  bindBackTap: function(){
    this.animation.width(0).step({ duration: 700 })
    this.setData({
      animationData: this.animation.export()
    })
  },
  bindBackTap1: function(){
    this.animation.width(0).step({ duration: 700 })
    this.setData({
      animationData1: this.animation.export()
    })
  },
  bindBrandTap: function(e) {  //  点击品牌
    var that = this;
    var _datasetId=e.target.dataset.id; 
    //console.log(_datasetId)
    that.setData({
      brandColor:_datasetId
    });
    wx.setStorageSync("brand",e.target.dataset.text)
    var brandVal = e.target.dataset.value;
    var brandText = e.target.dataset.text;
    wx.request({
      url: 'https://carprice.m.58.com/comm/chexi.json', 
      data: {
         cityid: '1' ,
         vid: brandVal,
         key:brandText
      },
      header: {
          'content-type': 'application/json'
      },
      success: function(res) {
         that.setData({
          chexiArray : res.data,
          chexiColor : -1
         })
         
      }
    })
    this.animation.width('582rpx').step({ duration: 700 })
    this.setData({
      animationData: this.animation.export()
    })
    
    //console.log("fff")
  },
  bindChexiTap:function(e){  ////  点击品牌
    var that = this;
    var chexiVal = e.target.dataset.value;
    var chexiText = e.target.dataset.text;
    var url = "https://carprice.m.58.com/comm/model.json";
    var _datasetId=e.target.dataset.id; 
    that.setData({
      chexiColor:_datasetId
    });
    wx.setStorageSync("chexi",e.target.dataset.text)
    wx.request({
      url: url, //点击车系，获取车型的接口
      data: {
         cityid: '1' ,
         vid: chexiVal,
         key:encodeURI(chexiText)
      },
      header: {
          'content-type': 'application/json'
      },
      success: function(res) {
         that.setData({
          chexingArray : res.data

         })
         //console.log(res.data[1].year)
      }

    })
    this.animation.width('382rpx').step({ duration: 700 })
    this.setData({
      animationData1: this.animation.export()
    })
  },
  bindChexingTap:function(e){  ////  点击品牌
    var that = this;
    var _datasetId=e.target.dataset.id; 
    that.setData({
      chexingColor:_datasetId
    });
    var eStr = e.target.dataset.text;
    var idx = eStr.lastIndexOf(" ")
    eStr = eStr.substring(0, idx)
    wx.setStorageSync("chexing",eStr)
    //console.log(e.target.dataset.text)
    wx.setStorageSync("chexingValue",e.target.dataset.value)
    var brand_txt = wx.getStorageSync("brand")+' '+ wx.getStorageSync("chexi")+' '+ wx.getStorageSync("chexing");
    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1];   //当前页面
    var prevPage = pages[pages.length - 2];  //上一个页面
    prevPage.setData({
      brandData:brand_txt  
    })
    wx.navigateBack({
      delta:1
    })

  },
  onShow: function(){
    var that = this;
    var animation = wx.createAnimation({
      transformOrigin: "50% 50%",
        duration: 1000,
        timingFunction: 'ease',
    })
    this.animation = animation
    wx.getSystemInfo({
      success: function(res) {
        //console.log(res)
        var height=res.windowHeight-40;   //footerpannelheight为底部组件的高度
        //console.log(height)
        that.setData({
            scrollHeight: height
        });
      }
    })
  },
  onLoad: function (options) {
    var that = this;
    var url = "https://carprice.m.58.com/comm/brand.json";
    wx.request({
      url: url, //点击车系，获取车型的接口
      data: {
         cityid: 1 
      },
      header: {
          'content-type': 'application/json'
      },
      success: function(res) {
         that.setData({
          brandArray : res.data,
          
         })
         var brandArray0 = res.data.splice(0,1);
         that.setData({
          brandArray : res.data,
          brandArray0:brandArray0
          
         })
      }

    })
    
    
  } 
})


