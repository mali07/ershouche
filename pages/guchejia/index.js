//index.js
//获取应用实例
var app = getApp()
Page({
  data: {
      array: ['请选择','美国', '中国', '巴西', '日本'],
      index: 0,
      endDate: '',
      brandData: '请选择',
      timeData: '请选择', 
      areaData: '请选择',
      distanceData:'',
      infoText:'',
      dateFlag:0,
      disFlag:1    
  },
  getCurrentDate: function() {
      var that = this;
      var date = new Date();
      var year = date.getFullYear();
      var month = date.getMonth() + 1;
      var strDate = date.getDate();
      if (month >= 1 && month <= 9) {
          month = "0" + month;
      }
      that.setData({
          endDate : year + '-' + month + '-' + strDate  
        
      })
   },   
  //事件处理函数
  bindBrandTap: function(){   //点击品牌框
    wx.navigateTo({
      url: '../brand/brand'
    })
  },
  bindDateTap: function(e){   //点击日期
    var that = this;
    that.setData({
      dateFlag : 1
    })
  },
  bindDateTap1: function(e){   //点击日期
    var that = this;
    wx.navigateTo({
      url: '../date/date'
    })
  },
  bindDistanceInput: function(e){    //输入里程获取焦点focus
    var that = this;
    var eValue = e.detail.value;
    eValue = eValue.replace(/[^\d.]/g, "");
    eValue = eValue.replace(/^\./g, "");
    eValue = eValue.replace(/\.{2,}/g, ".");
    eValue = eValue.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
    if(eValue>=100){
        eValue =  eValue.substr(0,2);
    }
    if(eValue.toString().substr(0,2)=="00"){
        eValue = "";
    }
    if(eValue.indexOf(".")=="-1"){
        eValue = eValue.replace(/^0([1-9]{1,5})$/g, parseInt(eValue));
    }
    //小数点后两位
    eValue = eValue.replace(/\.([0-9]{3,6})/g, eValue.substr(eValue.indexOf("."),3));
    return eValue;
  },
  bindDistanceBlur: function(e){
    var that = this;
    var eValue = e.detail.value;
    if (e.detail.value.lastIndexOf(".") == (e.detail.value.length - 1)) {
        e.detail.value = e.detail.value.substr(0, e.detail.value.length - 1);
        //console.log(e.detail.value)
    } else if (isNaN(eValue)) {
        eValue = " ";
    }
    that.setData({
      distanceData: eValue
    })
  },
  bindAreaTap: function(){   //点击所在城市
    wx.navigateTo({
      url: '../area/area'
    })
  },
  bindAssessTap: function(){   //点击马上评估

    var that = this;
    var distance = that.data.distanceData;
    if (distance == '' || distance == 0 ){
      wx.showToast({
        title: '请输入行驶历程',
        icon: 'none',
        duration: 2000
      })
      return
    }

    
    var chexingmodelid =  wx.getStorageSync("chexingValue");
    var date = wx.getStorageSync("year") + wx.getStorageSync("month");
    
    if(that.data.distanceData==""){
      var distance = 0;
    }else{
      var distance = that.data.distanceData.replace(".","_");
    } 
    var brandTxt = wx.getStorageSync("brand") + wx.getStorageSync("chexi") + wx.getStorageSync("chexing");
    var city = wx.getStorageSync("city");
    // wx.navigateTo({
    //   url: '../result/result?brandTxt='+brandTxt+'&cxing='+chexingmodelid+'&date='+date+'&distance='+distance+'&city='+city
    //   //url: '../result/result'
    // })

    wx.navigateTo({
      // url:'../newresult/newresult'
      url: '../newresult/newresult?brandTxt='+brandTxt+'&cxing='+chexingmodelid+'&date='+date+'&distance='+distance+'&city='+city
      //url: '../result/result'
    })

  },
  onLoad: function (options,tip) {
      var that = this;
      var sjzc_data = ['优车诚品','公平价','精真估'];
      function randomsort(a, b) {
          return Math.random() > .5 ? -1 : 1
      }
      var arr2 = sjzc_data.sort(randomsort);
      that.setData({
          infoText:arr2[0]+' '+arr2[1]+' '+arr2[2]
      })

  },
  onShareAppMessage: function () {  
    return {
      title: '58估车价', 
      path: '/pages/index/index'
    }
  },
  bindGoErshoucheList: function () {
    wx.navigateTo({
      url: '/pages/list/list',
      success: function(res){
        // success
      },
      fail: function() {
        // fail
      },
      complete: function() {
        // complete
      }
    })
  }
})
