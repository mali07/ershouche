var getRDData = require('../../data/get_rd_data.js');
var bmap = require('../../data/bmap-wx.js');
Page({
  data: {
    isShare: false,
    filter_datas : {
      isHide : true,
      filterInfos: [],
      filterUrl : '',
      total_datas : 0,
      currentCount: 0,
      filterForName : [],
      selectedText: {}
    },
    brand_datas : {
      brand_arrs:[],
      currentName : '',
      toView : '',
      hideBrand : true
    },
    series_datas : {
      series_arrs : [],
      hideSeries : true,
      currentSeries : '',
      brand_name : ''
    },
    letter_datas : {
      letter_arr : [],
      hideLetter: true
    },
    listInfos: [],
    info_scroll_fix: true,
    info_scroll_top : 0,
    loadMore : {
      pageNum: 1,
      totalPage: 1,
      nextPageUrl: '',
      haveMore: ''
    },
    sort_datas : {
      hideSort : true,
      hideSortMask : true,
      sortType: 'default',
    },
    recommend : null,
    tab_state:{
      cityName : '北京',
      city: 'bj',
      isOk : false
    },
    infoCountMap:{},
    filter_urls : [],
    more_btn : 0,
    startPos : {},
    curId:'',//当前点击帖子的infoid
    refer:'https://m.58.com/bj/ershouche/?listfrom=weixin&sort=default',
    referToList:'',
    localCity: null,
    infoids:'',
    openid:'',
    laiyuan:'',
    mp_openid:'',
    wxid:''

  },
  onLoad: function (option) {
    var _this = this;
    // wx.clearStorage()
   console.log(option);
    // 来源
    if (option && option.comefrom) {
      this.setData({
        comefrom: option.comefrom
      })
    }
    //设置来源
    if(option && option.ly){
      this.setData({
        laiyuan:option.ly
      })
    }
    if(option && option.laiyuan){
      this.setData({
        laiyuan:option.laiyuan
      })
    }
    if(option && option.refer){
      this.setData({
        laiyuan:option.refer
      })
    }
    if(option && option.wxid){
      this.setData({
        wxid:option.wxid
      })
    }
    if(option && option.openid){
      this.setData({
        mp_openid:option.openid
      })
    }
    try {
      wx.removeStorageSync('choosedCity')
    } catch (err) {
      console.log(err)
    }
    var filter_datas = {
      isHide : true,
      filterInfos: [],
      filterUrl: '',
      total_datas : 0,
      currentCount: 0,
      filterForName : [],
      selectedText: {}
    };
    // 获取城市数据
    var filterUrl = 'https://m.58.com/bj/ershouche/';
    // filterUrl=https%3A%2F%2Fm.58.com%2Fbj%2Ftuguancqj%2F%3Fsort%3Ddefault%26json%3Dtrue%26listfrom%3Dweixin&from=gzh
    // if (option && option.from == 'gzh' && option.filterUrl) { //来自公众号
    //   let url = decodeURIComponent(option.filterUrl)
    //   console.log(filterUrl)
    //   Promise.all([this.handleLocation(), this._fetchCityListData()]).then((res) => {
    //     console.log(res)

    //   }).catch(err => {  //定位失败/城市请求失败
    //     console.log('err', err)
    //   })
        
    // } else
    if (option && option.filterUrl && option.from == 'share') {  //来自分享
      filterUrl = option.filterUrl;
      filterUrl = decodeURIComponent(filterUrl);
      var city = filterUrl.match(/m.58.com\/([a-zA-Z]*)/)
      city = city[1]
      this.fillAllInfos(filterUrl, null, true)
      this._fetchCityListData().then(cityData => {
        var cityName = this._getNameOfCity(city)
        this.setData({
          tab_state: Object.assign(this.data.tab_state, {cityName: cityName, city: city}),
          isShare: true
        })
      }).catch(err => {
        console.log(errr)
      })
    } else {  // 定位
      Promise.all([this.handleLocation(), this._fetchCityListData()]).then((res) => {
        // console.log(res)
        
        let cityName = res[0] || ''
        var cityObj = this._getCityOfName(cityName)
        // console.log(cityObj)
        // cityObj = {
        //   cityId: '123',
        //   listName: 'tj',
        //   cityName: '天津',
        // }
        let url = ''
        if (option.from == 'gzh') { //来自公众号
          url = this._replaceCityOfUrl(decodeURIComponent(option.filterUrl), cityObj.listName)
        } else {
          url = this._replaceCityOfUrl('https://m.58.com/bj/ershouche/', cityObj.listName)
        }
        this.fillAllInfos(url, null, true)
        let tab_state = this.data.tab_state
        tab_state.cityName = cityObj.cityName
        tab_state.city = cityObj.listName
        this.setData({
          tab_state: tab_state
        })
      }).catch(err => {
        console.log('定位失败', err)
        let url = 'https://m.58.com/bj/ershouche/'
        if (option.from == 'gzh') {
         url = this._replaceCityOfUrl(decodeURIComponent(option.filterUrl), 'bj')
        }
        this.fillAllInfos(url, null, true)
        // this.setData({
        //   tab_state: Object.assign({
        //     cityName: '北京',
        //     city: 'bj'
        //   }, this.data.tab_state)
        // })
      })
    }

      this.setData({
      more_btn: 1,
      filter_datas: Object.assign(filter_datas, {filterUrl: filterUrl})
    })

    //获取全局openid
    try {
      var value = wx.getStorageSync('openid');
      if (value) {
        _this.setData({
          openid:value
        });
      }
    } catch (e) {
      console.log('error');
    }
  },
  onReady: function (option){
    var that = this;
    var url = this.data.filter_datas.filterUrl;
    if (!this.data.isShare) return ;
    //this.fillAllInfos(url, null,true);
    var urlObj = {
      isOk: true,
      urlVal: url
    }
   
    this.data.filter_urls.push(urlObj);
  },
  onShow: function (options) {
    let choosedCity = wx.getStorageSync('choosedCity') || ''
    if (!choosedCity) return ;
    wx.removeStorageSync('choosedCity')
    choosedCity = JSON.parse(choosedCity)
    let tab_state = this.data.tab_state
    tab_state.cityName = choosedCity.name
    tab_state.city = choosedCity.city
    this.setData({
      tab_state: tab_state
    })
    let filterData = this.data.filter_datas
     filterData.filterUrl = filterData.filterUrl.replace(/(m.58.com\/)([a-zA-Z]*)/i, `$1${choosedCity.city}`)
    this.setData({
      filter_datas: filterData
    })
    this.fillAllInfos(filterData.filterUrl, null, true)
  },
  fillAllInfos: function (url, sortVal,isStart){
    var that = this;
    var sortVal = sortVal || 'default'
    wx.showLoading({
      title: '加载中'
    })
    getRDData(url, { json: true, listfrom: 'weixin', sort: sortVal, openid: this.data.openid, ly: this.data.laiyuan, comefrom: this.data.comefrom}, function (data) {
      //筛选信息
      var filterInfos = data.filters;
      //列表信息
      var listInfos = data.queryResult.allInfos;
      //查询个数
      var total_datas = data.queryResult.totalCount;
      var currentCount = data.queryResult.currentCount;
      var allInfoNew = listInfos;
      //所有品牌信息
      var brand_datas = data.otherInfo.infoMap.zimuBrand;
      //页码信息&&加载更多
      var haveMore = data.queryResult.pageNum < data.queryResult.totalPage ? 'haveMore' : 'noMore';


      var infoids='';
      for (var i = 0; i < listInfos.length; i++) {
        var result = listInfos[i].infoMap;
        var price = result.price && result.price.substring(0, result.price.length - 1);
        var title = result.title.length > 28 ? result.title.substring(0, 28) + '...' : result.title;
        allInfoNew[i].infoMap.price = price;
        allInfoNew[i].infoMap.title = title;
        infoids += result.infoId + ',';
      }
      that.setData({
        infoids: infoids
      })
      var filterForName = [];
      var selectedText = {};
      for (var i = 0; i < filterInfos.length; i++) {
        var result = filterInfos[i];
        var forName = result.forName;
        filterForName.push(forName);
        if (isStart) {
          var subFilterLen = result.subFilter.length;
          if (subFilterLen >= 6) {
            selectedText[forName] = '更多';
          } else {
            selectedText[forName] = '';
          }
        }else{
          selectedText = that.data.filter_datas.selectedText;
        }
      }

      var letter_data = [];
      for (var i = 0; i < brand_datas.length; i++) {
        var result = brand_datas[i];
        letter_data.push(result.text);
      }

      var filter_datas = {
        isHide: true,
        filterInfos: filterInfos,
        total_datas: total_datas,
        currentCount: currentCount,
        filterUrl : url,
        filterForName: filterForName,
        selectedText: selectedText
      }
      var brand_arr = {
        brand_arrs: brand_datas,
        currentName: that.data.brand_datas.currentName,
        toView : '',
        hideBrand: true
      }
      var letter_datas =  {
        letter_arr: letter_data,
        hideLetter: true
      }

      var loadMore = {
        pageNum: data.queryResult.pageNum,
        totalPage: data.queryResult.totalPage,
        nextPageUrl: data.queryResult.nextPageUrl,
        haveMore : haveMore
      }
      var infoCountMap = {
        commoninfo: data.queryResult.infoCountMap.commoninfo,
        gulRecomLsnoInfo: data.queryResult.infoCountMap.gulRecomLsnoInfo ? data.queryResult.infoCountMap.gulRecomLsnoInfo : 0 
      }

      that.setData({
        listInfos: allInfoNew,
        filter_datas: filter_datas,
        brand_datas: brand_arr,
        letter_datas: letter_datas,
        info_scroll_fix: true,
        loadMore: loadMore,
        infoCountMap: infoCountMap
      });
      //设置这个滚动距离必须得等dom渲染完成才可以
      that.setData({
        info_scroll_top: 0
      })
      wx.hideLoading();
    })
  },
  changeFilter: function (url, tag, catename, text, sortVal,reset) {
    var that = this;
    var urlObj = {
      isOk : false,
      urlVal : url
    }
    this.data.filter_urls.push(urlObj);
    getRDData(url, { json: true, listfrom: 'weixin', sort: sortVal, openid: this.data.openid, ly: this.data.laiyuan, comefrom: that.data.comefrom}, function (data) {
      var total_datas = data.queryResult.totalCount;
      var currentCount = data.queryResult.currentCount;
      var filterInfos = data.filters;
      var selected = that.data.filter_datas.selectedText;
      var filterForName = that.data.filter_datas.filterForName;
      var selectedText = that.handleSelectedText(filterInfos);
      if (catename && text){
        selectedText[catename] = text;
      }
      if (reset && reset == 'reset'){
        for (var i in selectedText){
          if (selectedText[i] == ""){
            selectedText[i] = ''
          }else{
            selectedText[i] = '更多'
          }
        }
      }
      //醉了，这一块单独处理，其实是数据问题
      if (url.indexOf('pve_5864') > -1 && url.indexOf('pve_5883') > -1){
        var reg = /(\w|\/)+(pve_5864_(\d+_\d+))(\w|\/)+/;
        var arr = url.match(reg);
        var price = arr && arr[3];
        price = price.replace('_','-')+'万元';
        selectedText['MinPriceqj'] = price;
        var filterPrice = filterInfos.find(function (value, index, arr) {
          return value['forName'] == 'MinPriceqj';
        })
        filterPrice.subFilter.map(function (v, k) {
          if (v.text == '全部' && v.selected == true){
            filterInfos[4].subFilter[0].selected = false;
          }
        });
      }
      var filter_datas = {
        isHide: false,
        filterInfos: filterInfos,
        total_datas: total_datas,
        currentCount: currentCount,
        filterUrl : url,
        filterForName: that.data.filter_datas.filterForName,
        selectedText: selectedText
      }
      that.setData({
        filter_datas: filter_datas
      });
      //来自品牌车系
      if (tag && tag == 'brand'){
        that.closeBrand();
      }
    })
  },
  toDetail: function (event) {
    var formId=event.detail.formId;
    var datasets = event.currentTarget.dataset;
    var infoid = datasets.infoid;
    var data_url = datasets.url;
    var param_url = data_url;
    this.setData({
      curId: infoid
    });
    // if (data_url.indexOf('jingxuan') == -1) {
    //   var reg = /http:\/\/(\w+)\./;
    //   var city = data_url.match(reg)[1];
    //   param_url = 'https://m.58.com/' + city + '/ershouche/' + infoid + 'x.shtml';
    // } else {
    //   param_url = param_url.replace('http:', 'https:')
    // }
    // param_url = encodeURIComponent(param_url);
   // var url = '../detail/detail?params=' + param_url;

    var reg = /\/\/(\w+)\.\w+/;
    var arr = data_url.match(reg);
    if(arr && arr[1] != ''){
      var city = arr[1];
    }else{
      var city = this.data.tab_state.city;
    }

    var city=this.data.tab_state.city,refer=this.data.refer,cname=this.data.tab_state.cityName,weixinid=this.data.openid,mp_openid=this.data.mp_openid,wxid=this.data.wxid,
        laiyuan=this.data.laiyuan;
    var url = '../detail/detail?c=' + city +'&id='+infoid+'&cname='+cname+'&mp_openid='+mp_openid+'&wxid='+wxid+'&ly='+laiyuan+'&formId='+formId + '&comefrom=' + this.data.comefrom + '&formRefer='+refer;
    console.log('todetail', url)
    wx.navigateTo({
      url: url
    })
    var that=this;
    wx.reportAnalytics('list_click', {
      infoid: infoid,
      refer: refer,
      city_dw: cname,
      weixinid: that.data.openid,
      laiyuan: laiyuan,
    });
  },
  handleFilter: function () {
    var filter_urls = this.data.filter_urls;
    var that = this;
    var datas = {
      isHide: false,
      filterInfos: this.data.filter_datas.filterInfos,
      total_datas: this.data.filter_datas.total_datas,
      currentCount: this.data.filter_datas.currentCount,
      filterUrl: this.data.filter_datas.filterUrl,
      filterForName: this.data.filter_datas.filterForName,
      selectedText: this.data.filter_datas.selectedText
    }
    var url = this.data.filter_datas.filterUrl;
    //后端数据问题，前端单独处理
    if (url.indexOf('pve_5864') > -1 && url.indexOf('pve_5883') > -1) {
      var reg = /(\w|\/)+(pve_5864_(\d+_\d+))(\w|\/)+/;
      var arr = url.match(reg);
      if(!!arr){
        var price = arr && arr[3];
        price = price.replace('_', '-') + '万元';
        datas.selectedText['MinPriceqj'] = price;
        var filterPrice = datas.filterInfos.find(function (value, index, arr) {
          return value['forName'] == 'MinPriceqj';
        })
        filterPrice.subFilter.map(function (v, k) {
          if (v.text == '全部' && v.selected == true) {
            datas.filterInfos[4].subFilter[0].selected = false;
          }
        });
      }
    }
    this.setData({
      filter_datas: datas,
      info_scroll_fix: false
    });
    this.sortClose();
  },
  handleFilterItem: function (event){
    var that = this;
    var datasets = event.currentTarget.dataset; 
    var catename = datasets.catename;
    var text = datasets.text;
    var firstLetter = datasets.listname.charAt(0).toUpperCase();
    var url = datasets.url;
    var newUrl = that.dealUrl(url);
    var sortVal = that.data.sort_datas.sortType;
    if (catename == 'brand') {
      var brand_datas = {
        brand_arrs: this.data.brand_datas.brand_arrs,
        toView: firstLetter,
        currentName: text,
        hideBrand: false
      };
      if (text == '全部'){
        this.changeFilter(newUrl, null, catename, text, sortVal);
      }else{
        this.setData({
          brand_datas: brand_datas
        })
        this.fillSeries(newUrl, text);
      }
    }else{
      if (catename == 'fxcy' || catename == 'biansuq' || catename == 'biz'){
        this.changeFilter(newUrl, null, catename, '', sortVal);
      }else{
        this.changeFilter(newUrl, null, catename, text, sortVal);
      }
    }
  },
  handleClear : function(){
    var that = this;
    var url = 'https://m.58.com/bj/ershouche/';
    var sortVal = that.data.sort_datas.sortType;
    this.changeFilter(url, null, null, null, sortVal,'reset');
  },
  handleCheck : function(){
    var url = this.data.filter_datas.filterUrl;
    var sort = this.data.sort_datas.sortType;
    url=url.indexOf('?') == -1 ? url+'?listfrom=weixin' : url+'&listfrom=weixin';
    //重置refer
    this.setData({
      refer:url
    });
    this.fillAllInfos(url, sort, false);
    var urlObj = {
      isOk: true,
      urlVal: url
    };
    this.data.filter_urls.push(urlObj);
  },
  moreFilter: function (event){
    var that = this;
    var filter_urls = this.data.filter_urls;
    var datasets = event.currentTarget.dataset;
    var filterindex = datasets.filterindex;
    var catename = datasets.catename;
    if(catename == 'brand'){
      var brand_datas, letter_datas, series_datas;
      if (this.data.filter_datas.selectedText.brand == '更多' || this.data.filter_datas.selectedText.brand == '全部' || this.data.filter_datas.selectedText.brand == '全部/全部'){
        brand_datas = {
          brand_arrs: this.data.brand_datas.brand_arrs,
          currentName: '',
          toView: 'A',
          hideBrand: false
        };
        letter_datas = {
          letter_arr: this.data.letter_datas.letter_arr,
          hideLetter: false
        }
        series_datas = {
          series_arr: '',
          brand_name: '',
          currentSeries: '',
          hideSeries: true
        };
        this.setData({
          brand_datas: brand_datas,
          letter_datas: letter_datas,
          series_datas: series_datas
        })
      }else{
        var brandText = this.data.filter_datas.selectedText.brand;
        var reg = /([\u4e00-\u9fa5]|\w)+(\/)([\u4e00-\u9fa5]|\w)+$/g;
        var brandName, seriesName, brandArrs, seriesArr;
        if (reg.test(brandText)) {
          brandName = brandText.substring(0, brandText.indexOf('/'));
          seriesName = brandText.substring(brandText.indexOf('/') + 1, brandText.length);
        }
        var brand_datas = {
          brand_arrs: that.data.brand_datas.brand_arrs,
          currentName: that.data.brand_datas.currentName,
          toView: '',
          hideBrand: false
        };
        var letter_datas = {
          letter_arr: that.data.letter_datas.letter_arr,
          hideLetter: true
        }
        var series_datas = {
          series_arr: that.data.series_datas.series_arr,
          brand_name: that.data.series_datas.brand_name,
          currentSeries: that.data.series_datas.currentSeries,
          hideSeries: false
        };
        if (filter_urls.length > 1 && this.data.more_btn == 0) {
          for (var i = filter_urls.length - 1; i >= 0; i--) {
            if (filter_urls[i].isOk) {
              var url = filter_urls[i].urlVal;
              var params = {
                json: true,
                listfrom: 'weixin',
                sort: this.data.sort_datas.sortType,
                openid:this.data.openid,
                ly:this.data.laiyuan,
                comefrom:this.data.comefrom
              }
              getRDData(url, params, function (data) {
                var filterBrand = data.filters.find(function (value, index, arr) {
                  return value['forName'] == 'brand';
                })
                var filterBrandObj = filterBrand.subFilter.find(function (value, index, arr) {
                  return value['selected'] == true;
                })
                var firstLetter = filterBrandObj.listName.charAt(0).toUpperCase();
                var filterSeries = data.filters.find(function (value, index, arr) {
                  return value['forName'] == 'chexi';
                })
                seriesArr = filterSeries.subFilter;
                brand_datas = {
                  brand_arrs: that.data.brand_datas.brand_arrs,
                  currentName: brandName,
                  toView: firstLetter,
                  hideBrand: false
                };
                letter_datas = {
                  letter_arr: that.data.letter_datas.letter_arr,
                  hideLetter: true
                }
                series_datas = {
                  series_arr: seriesArr,
                  brand_name: brandName,
                  currentSeries: seriesName,
                  hideSeries: false
                };
                that.setData({
                  brand_datas: brand_datas,
                  letter_datas: letter_datas,
                  series_datas: series_datas,
                  more_btn: 1
                })
              })
              break;
            }
          }
        }
        that.setData({
          brand_datas: brand_datas,
          letter_datas: letter_datas,
          series_datas: series_datas
        })
      }
    }else{
      var filterForName = this.data.filter_datas.filterForName;
      if (filterForName[filterindex] != '') {
        filterForName[filterindex] = '';
      } else {
        filterForName[filterindex] = catename;
      }
      var datas = {
        isHide: false,
        filterInfos: this.data.filter_datas.filterInfos,
        total_datas: this.data.filter_datas.total_datas,
        currentCount: this.data.filter_datas.currentCount,
        filterUrl: this.data.filter_datas.filterUrl,
        filterForName: filterForName,
        selectedText: this.data.filter_datas.selectedText
      }
      this.setData({
        filter_datas: datas
      })
    }
    
  },
  handleBrand: function (event){
    var that = this;
    var datasets = event.currentTarget.dataset;
    var url = datasets.url;
    var brandName = datasets.text;
    var newUrl = this.dealUrl(url);
    var brand_datas = {
      brand_arrs: this.data.brand_datas.brand_arrs,
      toView : '',
      currentName: brandName,
      hideBrand: false
    };
    var letter_datas = {
      letter_arr: this.data.letter_datas.letter_arr,
      hideLetter: true
    }
    that.setData({
      brand_datas: brand_datas,
      letter_datas: letter_datas
    })

    that.fillSeries(newUrl, brandName);
  },
  brandScroll : function(){
    // var series_datas = this.data.series_datas;
    // series_datas['hideSeries'] = true;
    // this.setData({
    //   series_datas: series_datas
    // })
  },
  fillSeries: function (newUrl, brandName){
    var that = this;
    getRDData(newUrl, { filter: 0, json: true, listfrom: 'weixin', openid: this.data.openid, ly: this.data.laiyuan, comefrom:that.data.comefrom}, function (data) {
      var filters = data.filters;
      var series = null;
      for (var i = 0; i < filters.length; i++) {
        if (filters[i]['forName'] == 'chexi') {
          series = filters[i]['subFilter'];
        }
      }
      var datas = {
        series_arr: series,
        brand_name: brandName,
        currentSeries: '',
        hideSeries: false
      };
      that.setData({
        series_datas: datas
      })
    })
  },
  handleSeries : function(event){
    var datasets = event.currentTarget.dataset;
    var url = datasets.url;
    var newUrl = this.dealUrl(url);
    var currentSeries = datasets.text;
    var sortVal = this.data.sort_datas.sortType;
    var datas = {
      series_arr: this.data.series_datas.series_arr,
      brand_name: this.data.series_datas.brand_name,
      currentSeries: currentSeries,
      hideSeries: false
    };
    this.setData({
      series_datas: datas
    })
    var text = this.data.series_datas.brand_name + '/' + currentSeries;
    this.changeFilter(newUrl, 'brand', 'brand', text, sortVal, null);
  },
  handleLetter: function (event){
    var datasets = event.currentTarget.dataset;
    var text = datasets.text;
    var brand_datas = {
      brand_arrs: this.data.brand_datas.brand_arrs,
      currentName: this.data.brand_datas.currentName,
      toView: text,
      hideBrand: false
    };
    this.setData({
      brand_datas: brand_datas
    })
    wx.showToast({
      title: text,
      icon: 'none',
      duration: 1000
    })
  },
  backBrand:function(){
    this.closeBrand();
  },
  dealUrl : function(url){
     var newUrl = url;
     if(url.indexOf('http://m.58.com')>-1){
      newUrl = newUrl.replace('http', 'https');
     }
     var reg = /https:\/\/m.58.com/;
     newUrl = reg.test(newUrl) ? newUrl : 'https://m.58.com' + url;
     return newUrl;
  },
  handleMask: function () {
    var that = this;
    var filter_urls = this.data.filter_urls;
    var datas = {
      isHide: true,
      filterInfos: this.data.filter_datas.filterInfos,
      total_datas: this.data.filter_datas.total_datas,
      currentCount: this.data.filter_datas.currentCount,
      filterUrl: this.data.filter_datas.filterUrl,
      filterForName: this.data.filter_datas.filterForName,
      selectedText: this.data.filter_datas.selectedText
    }
    if (filter_urls.length > 1) {
      for (var i = filter_urls.length - 1; i >= 0; i--) {
        if (filter_urls[i].isOk) {
          var url = filter_urls[i].urlVal;
          var params = {
            json: true,
            listfrom: 'weixin',
            sort: this.data.sort_datas.sortType,
            openid:this.data.openid,
            ly:this.data.laiyuan,
            comefrom: this.data.comefrom
          }
          getRDData(url, params, function (data) {
            var filterInfos = data.filters;
            var filterForName = that.data.filter_datas.filterForName;
            var selectedText = that.handleSelectedText(filterInfos);
            var datas = {
              isHide: true,
              filterInfos: data.filters,
              total_datas: data.queryResult.totalCount,
              currentCount: data.queryResult.currentCount,
              filterUrl: url,
              filterForName: filterForName,
              selectedText: selectedText
            }
            that.setData({
              filter_datas: datas,
              info_scroll_fix: true
            });
            that.closeBrand();
          })
          break;
        }
      }
    }
    this.setData({
      filter_datas: datas,
      info_scroll_fix: true,
      more_btn: 0
    })
    this.closeBrand();
  },
  handleSelectedText: function (filterInfos){
    var selectedText = this.data.filter_datas.selectedText;
    for (var i = 0; i < filterInfos.length; i++) {
      var result = filterInfos[i];
      for (var j = 0; j < result.subFilter.length; j++) {
        var subRes = result.subFilter;
        if (result.forName == 'fxcy') {
          if (subRes[j].selected) {
            selectedText['service'] = subRes[j].text;
          }
        }else if(result.forName == 'biansuq'){
          if (subRes[j].selected) {
            selectedText['biansuq'] = subRes[j].text;
          }
        }else if(result.forName == 'biz'){
          if (subRes[j].selected) {
            selectedText['source'] = subRes[j].text;
          }
        } else if (result.forName == 'brand') {
          if (subRes[j].selected) {
            selectedText[result.forName] = subRes[j].text;
            selectedText['brandText'] = subRes[j].text;
          }
        } else if (result.forName == 'chexi') {
          if (subRes[j].selected) {
            selectedText['brand'] = selectedText['brand'] + '/' + subRes[j].text;
            selectedText['chexiText'] = subRes[j].text;
          }
        } else {
          if (subRes.length >= 6) {
            if (subRes[j].selected) {
              if (subRes[j].text == '全部') {
                selectedText[result.forName] = '更多';
              } else {
                selectedText[result.forName] = subRes[j].text;
              }
            }
          } else {
            selectedText[result.forName] = '';
          }
        }
      }
    }
    if (selectedText['brand'] == '全部/全部') {
      selectedText['brand'] = '更多';
    }
    return selectedText;
  },
  closeBrand : function(){
    var datas = {
      series_arr: this.data.series_datas.series_arr,
      brand_name: this.data.series_datas.brand_name,
      currentSeries: this.data.series_datas.currentSeries,
      hideSeries: true
    };
    var brand_datas = {
      brand_arrs: this.data.brand_datas.brand_arrs,
      currentName: this.data.brand_datas.currentName,
      toView : '',
      hideBrand: true
    };
    var letter_datas = {
      letter_arr: this.data.letter_datas.letter_arr,
      hideLetter: true
    }
    this.setData({
      series_datas: datas,
      brand_datas: brand_datas,
      letter_datas: letter_datas
    })
  },
  handleLower : function(){
    var that = this;
    var haveMore = this.data.loadMore.haveMore;
    var nextPageUrl = this.dealUrl(this.data.loadMore.nextPageUrl);
    var params = {};
    if(this.data.sort_datas.sortType == null){
      params = {
        json: true,
        listfrom: 'weixin',
        openid:this.data.openid
      }
    } else{
      params = {
        json: true,
        listfrom: 'weixin',
        sort: this.data.sort_datas.sortType,
        openid:this.data.openid,
        ly:this.data.laiyuan,
        comefrom:this.data.comefrom
      }
    }
    if (haveMore == 'haveMore'){
      getRDData(nextPageUrl, params , function (data) {
        var prevInfos = that.data.listInfos;
        var listInfos = data.queryResult.allInfos;
        var allInfoNew = listInfos;
        var infoids='';
        for (var i = 0; i < listInfos.length; i++) {
          var result = listInfos[i].infoMap;
          var price = result.price.substring(0, result.price.length - 1);
          var title = result.title.length > 28 ? result.title.substring(0, 28) + '...' : result.title;
          allInfoNew[i].infoMap.price = price;
          allInfoNew[i].infoMap.title = title;
          infoids += result.infoId + ',';
        }
        wx.reportAnalytics('list_nextpage', {
          infoid: infoids,
          refer: that.data.referToList,
          city_dw: that.data.tab_state.cityName,
          weixinid: that.data.openid,
          laiyuan: that.data.laiyuan
        });
        allInfoNew = prevInfos.concat(allInfoNew);
        var haveMore = data.queryResult.pageNum < data.queryResult.totalPage ? 'haveMore' : 'noMore';

        var loadMore = {
          pageNum: data.queryResult.pageNum,
          totalPage: data.queryResult.totalPage,
          nextPageUrl: data.queryResult.nextPageUrl,
          haveMore: haveMore
        }

        that.setData({
          listInfos: allInfoNew,
          loadMore: loadMore
        });
      })
    }
  },
  handleSort: function () {
    var currentState = this.data.sort_datas.hideSort;
    var sort_datas = {};
    var tab_state = this.data.tab_state;
    var sortType = this.data.sort_datas.sortType;
    if (currentState == true){
      sort_datas = {
        hideSort: false,
        hideSortMask: false,
        sortType: sortType
      }
      tab_state['isOk'] = true;
    }else{
      sort_datas = {
        hideSort: true,
        hideSortMask: true,
        sortType: sortType
      }
      tab_state['isOk'] = false;
    }
    this.setData({
      sort_datas: sort_datas,
      tab_state: tab_state
    })
  },
  sortClose : function(){
    var sort_datas = {
      hideSort: true,
      hideSortMask: true,
      sortType: this.data.sort_datas.sortType
    }
    var tab_state = this.data.tab_state;
    tab_state['isOk'] = false;
    this.setData({
      sort_datas: sort_datas,
      tab_state: tab_state
    })

  },
  sortFuncs: function (event){
    var datasets = event.currentTarget.dataset;
    var sortVal = datasets.type;
    //重置refer
    var refer=this.data.refer;
    refer=refer.replace(/(?![?&])(sort)=\w+/gi,'sort='+sortVal);
    this.setData({
      refer: refer
    });
    this.sortCont(sortVal);
    this.sortClose();
  },
  sortCont(sortVal){
    var that = this;
    var url = this.data.filter_datas.filterUrl;
    var params = {
      json : true,
      listfrom: 'weixin',
      sort: sortVal,
      openid:this.data.openid,
      ly:this.data.laiyuan,
      comefrom: this.data.comefrom
    }
    wx.showLoading({
      title: '加载中'
    })
    getRDData(url, params , function (data) {
      var listInfos = data.queryResult.allInfos;
      var allInfoNew = listInfos;
      for (var i = 0; i < listInfos.length; i++) {
        var result = listInfos[i].infoMap;
        var price = result.price.substring(0, result.price.length - 1);
        var title = result.title.length > 28 ? result.title.substring(0, 28) + '...' : result.title;
        allInfoNew[i].infoMap.price = price;
        allInfoNew[i].infoMap.title = title;
      }
      var sort_datas = {
          hideSort: true,
          hideSortMask : true,
          sortType: sortVal
      }

      that.setData({
        listInfos: allInfoNew,
        sort_datas: sort_datas
      });
      //设置这个滚动距离必须得等dom渲染完成才可以
      that.setData({
        info_scroll_top: 0
      })
      wx.hideLoading();
    })
  },
  //分享功能
  onShareAppMessage: function (res) {
    var filterUrl = "https://m.58.com/" + this.data.tab_state.city + '/ershouche?sort=' + this.data.sort_datas.sortType;
    var filter_urls = this.data.filter_urls;
    var isDefault = true
    if (filter_urls.length >= 1) {
      for (var i = filter_urls.length - 1; i >= 0; i--) {
        if (filter_urls[i].isOk) {
          filterUrl = filter_urls[i].urlVal;
          isDefault = false
          break;
        }
      }
    }
    var params = encodeURIComponent(filterUrl);
    var url = 'pages/list/list?filterUrl=' + params + '&from=share';
    if (res.from === 'button') {
      // 来自页面内转发按钮
      // console.log(res.target)
    }
    var selectObj = this.data.filter_datas.selectedText;
    var shareTxt = '';
    for(var i in selectObj){
      if (selectObj[i] != '更多' && selectObj[i] != ''){
        shareTxt += selectObj[i]+' ';
      }
    }
    return {
      title: shareTxt == "" ? '二手车推荐' : isDefault ? '二手车' : shareTxt,
      path: url,
      success: function (res) {
        wx.vibrateLong();
      },
      fail: function (res) {
        wx.showToast({
          title: '分享失败',
          icon: 'none',
          duration: 1000
        })
      }
    }
  },
  handleLocation: function () {
    var that = this;
    var BMap = new bmap.BMapWX({
      ak: '815kRNqEvZ7MdX3a4G0oFCGW4go8eQie'
    });
    return new Promise((resolve, reject) => {
      BMap.regeocoding({  
        success: function(data){
          var res = data.wxMarkerData[0].address;
          var cityName = "";
          if(res.indexOf('省')>-1){
            cityName = res.substring(0, res.indexOf('省'));
          }else{
            cityName = res.substring(0, res.indexOf('市'));
          }
          cityName = cityName.length > 4 ? cityName.substring(0, 4) : cityName;
          resolve(cityName)
        },
        fail: function (data) {
          // TODO 接口调用失败的处理
          console.log('接口调用失败', data)
          reject(data.errMsg)
        }
      });
    })
  
  },
  clickLocation : function () {
    wx.navigateTo({
      url: '../city/city?cityName=' + this.data.tab_state.cityName + '&city=' + this.data.tab_state.city
    })
  },
  filterStart:function(e){
    var startPos = {};
    startPos['x'] = e.changedTouches[0].pageX;
    startPos['y'] = e.changedTouches[0].pageY;
    this.setData({
      startPos: startPos
    })
  },
  filterEnd : function(e){
    var endPos = e.changedTouches[0];
    if ((endPos['pageX'] - this.data.startPos['x'] > 100) && (this.data.startPos['y'] - endPos['pageY'] < 50) && (this.data.startPos['y'] - endPos['pageY'] > -100)){
      this.handleMask();
    }
  }, 
  _fetchCityListData: function () {
    return new Promise((resolve, reject) => {
      try {
        var cityList = wx.getStorageSync('cityList') || ''
      } catch (err) {
        reject(err)
      }
      if (cityList !== '') {
        this.setData({localCity: JSON.parse(cityList)}, () => {
          resolve(cityList)
        })
      }
      var openid= wx.getStorageSync('openid');
      var url = 'https://cheapi.58.com/api/getDispLocalList';
      getRDData(url, { json: true, listfrom: 'weixin', sort: 'default', openid: openid, ly: this.data.laiyuan, comefrom: this.data.comefrom}, (data) => {
        if (data) {
          wx.setStorageSync('cityList', JSON.stringify(data.result))
          this.setData({localCity: data.result}, () => {
            // this._fetchListData()
            resolve(cityList)
          })
        }
      })

    }).catch(err => {
      console.log(err)
    })
   
  },
  _fetchListData: function () {
    var cityObj  = this._getCityOfName(this.data.tab_state.cityName)
    let tab_state = this.data.tab_state
    tab_state.city = cityObj.listName
    this.setData({
      tab_state: tab_state
    })
    let url = this.data.filter_datas.filterUrl.replace(/(m.58.com\/)([a-zA-Z]*)/i, `$1${cityObj.listName}`)
    this.fillAllInfos(url, null, true);
  },
  _getCityOfName: function (name) {
    let city = null;
    for(let k in this.data.localCity) {
      let citys = this.data.localCity[k]
      for(let i = 0; i < citys.length; i++) {
        if (citys[i].cityName == name) {
          city = citys[i]
        }
      }
    }
    return city
  },
  _getNameOfCity: function (city) {
    let cityName = null;
    for(let k in this.data.localCity) {
      let citys = this.data.localCity[k]
      for(let i = 0; i < citys.length; i++) {
        if (citys[i].listName == city) {
          cityName = citys[i].cityName
        }
      }
    }
    return cityName
  },
  _replaceCityOfUrl: function (url, city) {
    // console.log('erer', url, city)
    return url.replace(/(m.58.com\/)([a-zA-Z]*)/i, `$1${city}`)
  },
  goGuchejia: function () {
    wx.navigateTo({
      url: '/pages/guchejia/index',
      success: function(res){
        // success
        console.log('进入估车价')
      },
      fail: function() {
        // fail
      },
      complete: function() {
        // complete
      }
    })
  }
})