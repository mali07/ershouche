//index.js
//获取应用实例
var app = getApp()
Page({
  data: {
      array: ['请选择','美国', '中国', '巴西', '日本'],
      index: 0,
      areaStorage:"",
      provinceArray:"",
      cityArray:"",
      provinceColor:"-1",
      cityColor:"-1",
      scrollHeight:300,
      cityAnimation:{}
  },
  //事件处理函数
  bindProvinceTap: function(e) {  //  点击年限来获取月份
    var that = this;
    var provinceText = e.target.dataset.text;
    var provinceValue = e.target.dataset.value;
    var url = 'https://carprice.m.58.com/comm/city.json';
    var _datasetId=e.target.dataset.id; 
    that.setData({
      provinceColor:_datasetId
    });
    wx.setStorageSync("province",e.target.dataset.text)
    wx.request({
      url: url, //点击选择时间获取年份
      header: {
          'content-type': 'application/json'
      },
      data: {
          parentid:provinceValue
      },
      success: function(res) {
         that.setData({ 
          cityArray : res.data
          
         })
         
      }
    })
    that.animation.width('382rpx').step({ duration: 700 })
    that.setData({
      cityAnimation: this.animation.export()
    })  
  },
  //事件处理函数
  bindBackTap0: function(){
    wx.navigateBack({
      delta:1
    })
  },
  bindBackTap: function(){
    this.animation.width(0).step({ duration: 700 })
    this.setData({
      cityAnimation: this.animation.export()
    })
  },
  bindCityTap: function(e){
    var that = this;
    wx.setStorageSync("city",e.target.dataset.text);
    wx.setStorageSync("cityourparam",e.target.dataset.ourparam);
    var area_txt = wx.getStorageSync("city");
    var _datasetId=e.target.dataset.id; 
    that.setData({
      cityColor:_datasetId
    });
    
    /************利用栈来返回上一个页面 start**************/
    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1];   //当前页面
    var prevPage = pages[pages.length - 2];  //上一个页面
    prevPage.setData({
      areaData:area_txt  
    })
    wx.navigateBack({
      delta:1
    })
    /************利用栈来返回上一个页面 end**************/
  },
  onShow: function(){
    var that = this;
    var animation = wx.createAnimation({
        duration: 1000,
        timingFunction: 'ease',
    })
    that.animation = animation
    wx.getSystemInfo({
      success: function(res) {
        var height=res.windowHeight-40;   //footerpannelheight为底部组件的高度
        that.setData({
            scrollHeight: height
        });
      }
    })
  },
  onLoad: function (options) {
    var that = this;
    var url = 'https://carprice.m.58.com/comm/province.json'
    wx.request({
      url: url, //点击选择时间获取年份
      header: {
          'content-type': 'application/json'
      },
      success: function(res) {
         that.setData({ 
          provinceArray : res.data
          
         })
         
      }
    })
    
  } 
})


