//index.js
//获取应用实例
var app = getApp()
Page({
  data: {
    brandTxt:"",
    date:"",
    distance:"",
    city:"",
    priceArray:""
  },
  //事件处理函数
  bindAssessTap: function(e) {
    wx.clearStorage()
    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1];   //当前页面
    var prevPage = pages[pages.length - 2];  //上一个页面
    prevPage.setData({
      brandData:'请选择',
      timeData:'请选择',
      areaData: '请选择',
      disFlag:0,
      dateFlag:0
    })

    wx.navigateBack({
      delta:1
    })
  },
  onShareAppMessage: function () {  //58估车价分享
    return {
      title: '58估车价',
      path: '/pages/index/index'
    }
  },
  onLoad: function (options) {
    var that = this;

    that.setData({
      brandTxt:options.brandTxt,
      date:options.date,
      distance:options.distance,
      city:options.city,

    })
    var chexingValue = wx.getStorageSync("chexingValue");
    var date = wx.getStorageSync("year") + wx.getStorageSync("month");
    var date = date.split("年");
    var month = date[1].split("月");
    if(month[0].length==1){
      month[0] = '0'+month[0]
    }
    var date = date[0]+month[0];
    if(options.distance==""){
      var distance = 0;
    }else{
      var distance = options.distance.replace(".","_");
    }   
    var cityourparam =  wx.getStorageSync("cityourparam")
    //console.log(distance)
    var url = 'https://carprice.m.58.com/m'+chexingValue+'/d'+date+'m'+distance+'c'+cityourparam+'.json?caller=21';
    //var url = 'https://carprice.m.58.com/m681922/d201410m3c37.json?caller=21'
    console.log(url)
    wx.request({  //获取估价信息
      url: url, 
      header: {
          'content-type': 'application/json'
      },
      success: function(res) {
         that.setData({ 
          priceArray : res.data
         })
         
      }
    })
  }
})
