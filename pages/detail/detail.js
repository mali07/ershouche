var getRDData = require('../../data/get_rd_data.js');
Page({
  data: {
    url : '',
    infoid: '33243048835517',
    city : 'bj',
    banner_datas:{
      imgUrls: [],
      duration: 300,
      current : 1,
      total : 1
    },
    signature : '222',
    baseinfo_datas : {},
    detailinfo_datas : {},
    saleinfo_datas : {},
    guesslike_datas : null,
    refer:'',
    city_dw:'',
    openid:'',
    laiyuan:'',
    wxid:'',
    chexing:'',
    cityid:'',
    mp_openid:'',
    formId:''
  },
  onLoad: function (option) {
    console.log('detail', option);
    var scene = decodeURIComponent(option.scene);
    var that = this;
    var city = this.data.city;
    var infoid = this.data.infoid;
    var value=wx.getStorageSync('openid');
    if (scene !='undefined') {
      scene = scene.split('&');
      city = scene[0], infoid = scene[1];
      that.setData({
        laiyuan:scene[2]
      })
    } else {
      city = option.c, infoid = option.id;
    }
    //设置来源
    var ly='';
    if(option && option.ly){
      ly=option.ly;
      this.setData({
        laiyuan:option.ly
      })
    }
    if(option && option.laiyuan){
      ly=option.laiyuan;
      this.setData({
        laiyuan:option.laiyuan
      })
    }
    if(option && option.refer){
      ly=option.refer;
      this.setData({
        laiyuan:option.refer
      })
    }
    if(option && option.mp_openid){
      this.setData({
        mp_openid:option.mp_openid
      })
    }
    if(option && option.openid){
      this.setData({
        mp_openid:option.openid
      })
    }
    if(option && option.wxid){
      this.setData({
        wxid:option.wxid
      })
    }
    if(option && option.formId){
      var formId=option.formId;
      this.setData({
        formId:option.formId
      })
    }
    if (option && option.comefrom) {
      this.setData({
        comefrom: option.comefrom
      })
    }

    var url = 'https://m.58.com/' + city + '/ershouche/' + infoid + 'x.shtml?listfrom=weixin' + '&openid=' + value + '&formId=' + formId + '&ly=' + ly + '&comefrom='+option.comefrom;
    that.fillAllInfo(url);
    that.setData({
      url: url,
      refer:option.formRefer,
      city_dw:option.cname,
      city:city
    });
    //that.fillAllInfo(url);
    wx.getUserInfo({
      success: function (res) {
        that.setData({
          signature: res.signature
        })
      }
    });
    wx.showLoading({
      title: '加载中'
    })
    //获取全局openid
    try {
      var value = wx.getStorageSync('openid');
      if (value) {
        that.setData({
          openid:value
        });
      }
    } catch (e) {
      console.log('error');
    }
  },
  onReady : function(){
    setTimeout(function () {
      wx.hideLoading();
    }, 1000)
  },
  fillAllInfo : function(url){
    var that = this;
    getRDData(url, { json: true, comefrom: that.data.comefrom} , function(data){
      //轮播图
      var banner_datas = data.baseInfo.pics;
      var new_banners = banner_datas;
      for(var i = 0;i<banner_datas.length;i++){
        new_banners[i] = new_banners[i] +'?w=525&h=392&crop=1';
        //new_banners[i] = new_banners[i] + '?w=750&h=560&crop=1';
      }
      that.fillBanner(banner_datas);

      //基本信息
      that.fillBaseInfo(data);
      //详细信息
      that.fillDetailInfo(data.cateInfo.infoMap);
      //卖家信息和图片
      that.fillSaleInfo(data);
      //加载猜你喜欢，必须拿到InfoID
      that.fillRecommend(data.baseInfo.infoId);
      that.setData({
        infoid: data.baseInfo.infoId
      })
      //参数配置页面
      that.setData({
        chexing: data.cateInfo.infoMap.chexingValue,
        cityid: data.baseInfo.dispLocal.localID
      })

      //wx.hideLoading();
    });
  },
  fillBanner: function (banner_datas){
    var imgUrls = banner_datas;
    var total = banner_datas.length;
    var banner_datas =  {
      imgUrls: imgUrls,
      duration: 300,
      current: 1,
      total: total
    }
    this.setData({
      banner_datas: banner_datas
    })
  },
  fillBaseInfo : function(data){
    var baseinfo_datas = data.baseInfo;
    var price = baseinfo_datas.price == '' ? '面议' : baseinfo_datas.price;
    var title = baseinfo_datas.title.length > 42 ? baseinfo_datas.title.substring(0, 42) + '...' : baseinfo_datas.title;
    var brandImg = data.cateInfo.infoMap.brandImg;
    var haveTax = false;
    var TaxPrice = data.cateInfo.infoMap.TaxPrice ? data.cateInfo.infoMap.TaxPrice : "";
    if (data.cateInfo.infoMap.guohufeiyong && data.cateInfo.infoMap.guohufeiyong == '553663') {
      haveTax = true;
    };
    baseinfo_datas = {
      price: price,
      title: title,
      brandImg: brandImg,
      haveTax: haveTax,
      TaxPrice: TaxPrice
    }
    this.setData({
      baseinfo_datas: baseinfo_datas
    })
  },
  fillDetailInfo: function(data){
    var detailinfo_datas = {
      maininfo: data.basicInfo1,
      otherinfo: data.basicInfo2
    }
    this.setData({
      detailinfo_datas: detailinfo_datas
    })
  },
  fillSaleInfo : function(data){
    var that = this;
    var pics = this.data.banner_datas.imgUrls;
    var goblianxiren = data.userInfo.goblianxiren;
    var localName = data.baseInfo.dispLocal.localName;
    var personImg = 'http://pic1.58cdn.com.cn/p1/n_v1bkuyfvjvsxlfppwjc5mq.png';
    var content = data.baseInfo.content.indexOf('<br') > 0 ? data.baseInfo.content.replace(/<br \/>/g, '') : data.baseInfo.content;
    var isOk = false;
    var content_brief = "";
    if (content.length > 200){
      isOk = true;
      content_brief = content.substring(0, 200) + '...';
    }
    var openid=that.data.openid,
        formId=that.data.formId,
        ly=that.data.laiyuan;
    var user_url = "https://cheapi.58.com/api/personal/" + data.userInfo.userId + '?openid=' + openid + '&formId=' + formId + '&ly=' + ly + '&comefrom=' + that.data.comefrom;
    getRDData(user_url, function (data) {
      if(data.status == 0){
        personImg = data.result.headImgUrl;
        if (personImg != ""){
          var saleinfo_datas = {
            goblianxiren: goblianxiren,
            localName: localName,
            personImg: personImg,
            content: content,
            content_brief: content_brief,
            isOk: isOk,
            pics: pics
          }
          that.setData({
            saleinfo_datas: saleinfo_datas
          })
        }
      }
    })
    var saleinfo_datas = {
      goblianxiren: goblianxiren,
      localName: localName,
      personImg: personImg,
      content: content,
      content_brief: content_brief,
      isOk: isOk,
      pics: pics
    }
    that.setData({
      saleinfo_datas: saleinfo_datas
    })
  },
  fillRecommend : function(infoid){
    var that = this;
    var openid=that.data.openid,
        formId=that.data.formId,
        ly=that.data.laiyuan,
        comefrom = that.data.comefrom;
    var tuijian_link_chexi = "https://cheapi.58.com/api/getPcRightTuijian?infoId=" + infoid + "&count=8&range=chexi&pageId=45" + '&openid=' + openid + '&formId=' + formId + '&ly=' + ly + '&comefrom=' + comefrom;
    var tuijian_link_price = "https://cheapi.58.com/api/getPcRightTuijian?infoId=" + infoid + "&count=8&range=price&pageId=45" + '&openid=' + openid + '&formId=' + formId + '&ly=' + ly + '&comefrom=' + comefrom;
    var allInfo = [];
    getRDData(tuijian_link_chexi, function (data) {
      if(data.status == 0){
        var result1 = data.result;
        allInfo = allInfo.concat(result1);
      }
      getRDData(tuijian_link_price, function (data) {
        if (data.status == 0) {
          var result2 = data.result;
          allInfo = allInfo.concat(result2);
          that.setData({
            guesslike_datas: allInfo
          })
        }
      })
      that.setData({
        guesslike_datas: allInfo
      })
      //console.log(allInfos);
    })
    
    
  },
  makePhoneCall : function(e){
    console.log('form发生了submit事件，携带数据为：', e.detail);
    var formId=e.detail.formId;
    var infoid = this.data.infoid;
    var signature = this.data.signature;
    var wxid = this.data.wxid;
    var openid = this.data.openid;
    var mp_openid = this.data.mp_openid;
    var ly = this.data.laiyuan;
    var comefrom = this.data.comefrom;
    var tel_url = "https://cheapi.58.com/api/detail/phone/get/" + infoid + "?clientType=2&deviceId=" + signature + "&tinyapp=true&spm=ershouchezhuzhantinyapp&wxid=" 
        + wxid + '&openid=' + openid + '&mp_openid=' + mp_openid+'&ly='+ly+'&formId='+formId + '&comefrom=' + comefrom;
      
    console.log('tel_url', comefrom, tel_url);
    //var tel_url = "https://wxapp.58.com/phone/get?infoId=" + infoid + "&cateCode=6&thirdKey=mA3JpELH1Th7OrcqVC48L1a7IznUSTM5ITZLRPBQiaCCNwmib8e9wrpJnTpYdAG7&appCode=0";
    getRDData(tel_url, function (data) {
      if (data.status == 1){   
          var call_num = data.result;
          if (call_num) {
            wx.makePhoneCall({
              phoneNumber: call_num
            });
          }
      }
    })
  },
  loadPage : function(event){
    var datasets = event.currentTarget.dataset;
    var infoid = datasets.infoid;
    var url = datasets.url;
    var city = '';
    if(url.indexOf('jingxuan') == -1){
      var reg = /http:\/\/(\w+)\./;
      var city = url.match(reg)[1];
      url = 'https://m.58.com/' + city + '/ershouche/' + infoid + 'x.shtml';
    }else{
      url = url.replace('http:','https:')
    }
    url = encodeURIComponent(url);
    city=this.data.city;
    var url = './detail?c=' + city + '&id=' + infoid;
    wx.navigateTo({
      url: url
    })
  },
  bannerChange : function(event){
    var current = event.detail.current+1;
    var banner_datas = {
      imgUrls: this.data.banner_datas.imgUrls,
      duration: 300,
      current: current,
      total: this.data.banner_datas.total
    }
    this.setData({
      banner_datas: banner_datas
    })

  },
  showAllInfo : function(){
    var saleinfo_data = {
      goblianxiren: this.data.saleinfo_datas.goblianxiren,
      localName: this.data.saleinfo_datas.localName,
      personImg: this.data.saleinfo_datas.personImg,
      content: this.data.saleinfo_datas.content,
      content_brief: this.data.saleinfo_datas.content_brief,
      isOk: false,
      pics: this.data.saleinfo_datas.pics
    }
    this.setData({
      saleinfo_datas: saleinfo_data
    })
  },
  hrefParamsPage : function(){
    let chexing = this.data.chexing;
    let cityid = this.data.cityid;
    wx.navigateTo({
      url: '../params/params?chexing=' + chexing + '&cityid=' + cityid
    })
  },
  //分享功能
  onShareAppMessage: function (res) {
    var url = this.data.url;
    url = encodeURIComponent(url);
    var title = this.data.baseinfo_datas.title;
    var city=this.data.city,infoid=this.data.infoid;
    return {
      title: title,
      path: 'pages/detail/detail?c=' + city + '&id=' + infoid,
      success: function (res) {
        wx.showToast({
          title: '分享成功',
          icon: 'success',
          duration: 1000
        });
        wx.vibrateLong();
      },
      fail: function (res) {
        wx.showToast({
          title: '分享失败',
          icon: 'none',
          duration: 1000
        })
      }
    }
  }
})