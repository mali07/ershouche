var getRDData = require('../../data/get_rd_data.js')
Page({
  data: {
    chooseCity: null,
    searchValue: '',
    isShowBlurCity: false,
    presentCity: '',
    commonCity: [],
    nearCity: [],
    localCity: {},
    blurCity: [],
    letter_arr:[],
    hideLetter: false,
    activeLetter: '',
    letterCityList: [],
    letter: {
      height: '',
      startY: 0,
    },
    test_err: '',
  },
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    
    this.setData({
      presentCity: {
        cityName: options.cityName || '',
        city: options.city || ''
      }
    })
    this._fetchNearbyCityData();
    this._fetchCityListData();
    try {
      var commonCitys = wx.getStorageSync('commonCity') || '';
    } catch (err) {
      this.setData({
        test_err: JSON.stringify(err)
      })
    }
    commonCitys = commonCitys ? JSON.parse(commonCitys) : []
    this.setData({
      commonCity: commonCitys.reverse(),
      letter_arr: Object.keys(this.data.localCity),
    });
  },
  inputFocus: function () {
    this.setData({isShowBlurCity: true})
  },
  hideBlurCity: function () {
    this.setData({
      isShowBlurCity: false,
      searchValue: '',
      blurCity: []
    })
  },
  inputChange: function (event) {
    var {value} = event.detail;
    if (value) {
      this.fetchBlurCityData(value)
    } else {
      this.setData({
        blurCity: []
      })
    }
  },
  clearInput: function (ev) {
    this.setData({
      searchValue: '',
      blurCity: []
    })
  }, 
  _fetchNearbyCityData: function () {
    var url = 'https://cheapi.58.com/api/getNearbyCity/' + this.data.presentCity.city;
    getRDData(url, { json: true, listfrom: 'weixin', sort: 'default'},  (data) => {
      if (data && data.result && data.result.length > 0) {
        wx.hideLoading()
        this.setData({nearCity: data.result})
      }
    })
  },
  _fetchCityListData: function () {
    try {
      var cityList = wx.getStorageSync('cityList') || ''
    } catch (err) {
       this.setData({
         test_err: JSON.stringify(err)
       })
    }
    if (cityList) {
      this.setData({
        'localCity': JSON.parse(cityList)
      }, () => {
        this._initLetter()
      })
      return void 0;
    }
    var url = 'https://cheapi.58.com/api/getDispLocalList'
    getRDData(url, {json: true, listfrom: 'weixin', sort: 'default'}, (data) => {
      this.setData({localCity: data.result})
      this.setData({letter_arr: Object.keys(data.result)}, () => {
        this._initLetter()
      })
    })
  },
  fetchBlurCityData: function (key) {
    var url = `https://cheapi.58.com/api/getSouCity?key=${key}`;
    getRDData(url, {json: true, listfrom: 'weixin', sort: 'default'}, (data) => {
      this.setData({blurCity: data.result})
    })
  },
  chooseCity: function (event) {
    var id = this._getKeyOfEvent('id', event)
    var name = this._getKeyOfEvent('name', event)
    var city = this._getKeyOfEvent('city', event)
    this.setData({
      chooseCity: {id, name, city}
    });
    this._goList()
  },
  chooseBlurCity: function (event) {
    let name = this._getKeyOfEvent('name', event).trim();
    let city = this._getCityOfName(name);
    city && this.setData({
      chooseCity: {
        id: city.cityId,
        name: city.cityName,
        city: city.listName
      }
    })
    this._goList()
  },
  _goList: function () {
    let chooseCity = this.data.chooseCity;
    let str = wx.getStorageSync('commonCity') || '' 
    let commonCitys =  str && JSON.parse(str) || []
    // 验证城市是否存在
    let isRepeat = -1
    for (let i = 0; i < commonCitys.length; i++) {
      if (chooseCity.id == commonCitys[i].id) {
        isRepeat = i
      }
    }
    try {
      if (isRepeat == -1 && commonCitys.length == 8) { // 没有重复的
        let city = commonCitys.shift()
      } else if (isRepeat !== -1) {
        commonCitys.splice(isRepeat, 1)
      }
      commonCitys.push(chooseCity)
      wx.setStorageSync('commonCity', JSON.stringify(commonCitys));
      wx.setStorageSync('choosedCity', JSON.stringify(chooseCity));
    } catch (err) {
      this.setData({
        test_error: JSON.stringify(err)
      })
    }

    // 跳转至列表页
    wx.navigateBack({
      url: `/pages/list/list`
    })
  },
  _getKeyOfEvent: function (key, item) {
    var res = undefined
    if (item.currentTarget && item.currentTarget.dataset) {
      res = item.currentTarget.dataset[key]
    }
    return res;
  },
  _initLetter: function () {
    var that = this
    var query = wx.createSelectorQuery()
    query.selectAll('.letter').fields({rect: true}, function (rects) {
      var letter = that.data.letter
      letter.top = rects[0] ? rects[0].top : 0;
      that.setData({
        letter:letter
      })
    }).exec();
    query.selectAll('.letter-cont').fields({size: true}, function (rects) {
      var letter = that.data.letter
      letter.height = rects[0] && rects[0].height
      that.setData({
        letter: letter
      })
    }).exec(); 
    query.selectAll('.letter-city').fields({size: true}, function (rects) {
      var letter = that.data.letter
      letter.cityList = rects
      that.setData({
        letter: letter
      })
    }).exec();
  },
  _getCityOfName: function (name) {
    let city = null;
    for(let k in this.data.localCity) {
      let citys = this.data.localCity[k]
      for(let i = 0; i < citys.length; i++) {
        if (citys[i].cityName == name) {
          city = citys[i]
        }
      }
    }
    return city
  },
  touchStart: function (event) {
    // this._initLetter()
    var that = this
    var clientY = event.touches[0].clientY || 0
    var key = event.currentTarget.dataset.text
    var letter = this.data.letter
    letter.startY = clientY
    
    this.setData({
      letter: letter,
      activeLetter: key
    })
  },
  touchMove: function (event) {
    var letter = this.data.letter
    var moveY = event.touches[0].clientY - letter.top
    var index = Math.floor(moveY/this.data.letter.height)
    var distance = 0;
    index = Math.max(0, Math.min(index, this.data.letter_arr.length-1))
    wx.showToast({
      title: this.data.letter_arr[index],
      icon: 'none',
      duration: 700
    })
    this.setData({
      activeLetter: this.data.letter_arr[index]
    })
  }
}) 