//index.js
//获取应用实例
var app = getApp()
Page({
  data: {
      array: ['请选择','美国', '中国', '巴西', '日本'],
      index: 0,
      dateData:"请选择",
      yearArray:"",
      monthArray:"",
      yearColor:"-1",
      monthColor:"-1",
      monthAnimation:{}

  },
  //事件处理函数
  bindYearTap: function(e) {  //  点击年限来获取月份
    var that = this;
    var yearText = e.target.dataset.text;
    var currentYear = new Date().getFullYear();
    var currentMonth = new Date().getMonth()+1;
    var _datasetId=e.target.dataset.id; 
    //console.log(_datasetId)
    that.setData({
      yearColor:_datasetId
    });
    //console.log(that.data.yearColor)
    wx.setStorageSync("year",e.target.dataset.text)
    currentYear = currentYear.toString()+"年";
    //console.log(yearText)
    //console.log(currentYear)
    var monthArray1=[]
    for(var i = currentMonth;i>0;i--){
      monthArray1.push(i)
      //console.log(monthArray1)
    }
    that.setData({
      monthArray1:monthArray1
    })
    //console.log(monthArray1)
    if(currentYear==yearText){
        that.setData({
          monthArray:monthArray1
        })
    }else{
        that.setData({
          monthArray:[12,11,10,9,8,7,6,5,4,3,2,1]
        })
    }
    that.animation.width('382rpx').step({ duration: 700 })
    that.setData({
      monthAnimation: this.animation.export()
    })
  },
  bindMonthTap: function(e){
    var that = this;
    wx.setStorageSync("month",e.target.dataset.text)
    var time_txt = wx.getStorageSync("year") + wx.getStorageSync("month");
    var _datasetId=e.target.dataset.id; 
    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1];   //当前页面
    var prevPage = pages[pages.length - 2];  //上一个页面
    prevPage.setData({
      timeData:time_txt  
    })
    wx.navigateBack({
      delta:1
    })
    //console.log(_datasetId)
    that.setData({
      monthColor:_datasetId
    });
  },
  bindBackTap0: function(){
    wx.navigateBack({
      delta:1
    })
  },
  bindBackTap: function(){
    this.animation.width(0).step({ duration: 700 })
    this.setData({
      monthAnimation: this.animation.export()
    })
  },
  onShow: function(){
    var animation = wx.createAnimation({
        duration: 1000,
        timingFunction: 'ease',
    })
    this.animation = animation
  },
  onLoad: function (options) {
    var that = this;
    var url = 'https://carprice.m.58.com/comm/regdate.json'
    wx.request({
      url: url, //点击选择时间获取年份
      data: {
         cityid: '1' ,
         vid: wx.getStorageSync('chexingValue'),
         key:wx.getStorageSync('chexing')
      },
      header: {
          'content-type': 'application/json'
      },
      success: function(res) {
        //console.log(res.data)
         that.setData({ 
          yearArray : res.data.reverse()
         })
         
      }
    })
   
    
  } 
})


