var md5 = require('../md5/md5.js')

function formatTime(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()


  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}

// 小数点后两位百分比
function toPercent(num, total) {
  var _num = parseFloat(num);
  var _total = parseFloat(total);
  return (Math.round(_num / _total * 10000) / 100.00 + "%");
}

//md5 加密
function md5_cmn(str) {
  // String passStr = MD5(source).substring(8, 24);
  // //		System.out.println("" + passStr);
  // String newPassStr = "58gcj" + passStr + "app";
  var _str = md5.hex_md5(str).substring(8, 24);
  return md5.hex_md5("58gcj" + _str + "app")
}

//拨打电话
function makePhoneCall(num) {
  wx.makePhoneCall({
    phoneNumber: num //仅为示例，并非真实的电话号码
  })
}

//判断是否是ios平台

function isiOS(){
  var isiOS = !!0;
  wx.getSystemInfo({
    success: function (res) {
      // console.log(res.model)
      // console.log(res.model.indexOf('iPhone'))
      if (res.model.indexOf('iPhone') != -1){
        isiOS = !!1;
      }
    }
  })
  return isiOS;
};


module.exports = {
  formatTime: formatTime,
  toPercent: toPercent,
  md5_cmn: md5_cmn,
  makePhoneCall: makePhoneCall,
  isiOS: isiOS
}
