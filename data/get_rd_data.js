function getRDData(url,data,callback){
  if(typeof data == 'function'){
    callback = data;
    data = null;
  }
  wx.request({
    url: url, //仅为示例，并非真实的接口地址
    data: data,
    success: function (res) {
      // if (res.data.status && res.data.status !== 0 && res.data.status !== 1) {
      //   wx.showToast({
      //     title: res.data.msg || '',
      //     icon: 'none',
      //     duration: 1000
      //   })
      // } else {
      //   callback(res.data)
      // }
      callback(res.data)
    },
    fail : function(){
      wx.showToast({
        title: '数据请求有问题，请刷新重试',
        icon: 'none',
        duration: 1000
      })
    }
  })
}

module.exports = getRDData;


